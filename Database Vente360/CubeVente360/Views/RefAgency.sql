﻿


CREATE VIEW [CubeVente360].[RefAgency]
AS
/****** Script de la commande SelectTopNRows à partir de SSMS  ******/
SELECT  [skeyRefAgency]
      ,[RefAgencySourceOpe]
      ,[RefAgencyOpeSk]
      ,[RefAgencyOpeID]
      ,[RefAgencyOpeName]
      ,[RefAgencySapOpeID]
      ,[RefAgencyLastUpdateOpeDate]
      ,[skeyAgency]
     
  FROM [BI_DATAQUALITY].[ref].[DimRefAgency]
  where scd_current= 1