﻿
CREATE VIEW [CubeVente360].[Area]
AS
SELECT skeyArea, AreaID, AreaName, MarketID
FROM   BI_DWH_AFFICHAGE.dwh.DimArea
GO
EXECUTE sp_addextendedproperty @name = N'MS_DiagramPaneCount', @value = 1, @level0type = N'SCHEMA', @level0name = N'CubeVente360', @level1type = N'VIEW', @level1name = N'Area';




GO