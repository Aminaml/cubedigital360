﻿


CREATE VIEW [CubeVente360].[RefSalesRep]
AS
/****** Script de la commande SelectTopNRows à partir de SSMS  ******/
SELECT [skeyRefSalesRep]
      ,[RefSalesRepSourceOpe]
      ,[RefSalesRepSalesRepOpeSK]
      ,[RefSalesRepSalesRepOpeID]
      ,[RefSalesRepSalesRepOpeName]
      ,[RefSalesRepPhoneOpe]
      ,[RefSalesRepPhoneMobileOpe]
      ,[RefSalesRepEmailOpe]
      ,[RefSalesRepSapOpeID]
      ,[RefSalesRepLastDateUpdateOpe]
      ,[skeySalesRep]
     
  FROM [BI_DATAQUALITY].[ref].[DimRefSalesRep]

  where scd_current = 1