﻿
CREATE VIEW cubevente360.[NumediaProduct]
AS
SELECT skeyDimNumediaProduct, NumediaProductID, NumediaProductDes, NumediaProductDes_fr, StartDate, EndDate
FROM   BI_DWH_AFFICHAGE.dwh.DimNumediaProduct
WHERE (isCurrent = 1)