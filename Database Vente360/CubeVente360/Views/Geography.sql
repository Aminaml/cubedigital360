﻿
create VIEW cubevente360.[Geography]
AS
SELECT SkeyGeography, GeographyId, City, StateCode, StateName, CountryCode, Country
FROM   BI_DATAQUALITY.mdm.DimGeography
WHERE (scd_current = 1)