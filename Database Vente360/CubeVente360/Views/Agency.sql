﻿
CREATE VIEW [CubeVente360].[Agency]
AS

SELECT  [skeyAgency]
      ,[AgencySapID]
      ,[AgencyName1]
      ,[AgencyName2]
      ,[AgencyCity]
      ,[AgencyStreet]
      ,[AgencyPostCode]
      ,[AgencyRegion]
      ,[AgencyCountry]
      ,[AgencyPhone1]
      ,[AgencyFaxNumber]
      ,[AgencyGroup1Sap]
      ,[AgencyGroup1]
      ,[AgencyGroup2]
      ,[AgencyGroup3]
      ,[AgencyGroup4]
      ,[AgencyGroup5]
      
  FROM [BI_DATAQUALITY].[mdm].[DimAgency]