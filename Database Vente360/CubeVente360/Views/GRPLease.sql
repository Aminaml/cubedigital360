﻿


CREATE VIEW [CubeVente360].GRPLease
AS
SELECT [StartFiscalWeek]
      ,[EndFiscalWeek]
	  ,convert(varchar, StartFiscalWeek, 112) as DATEKEY
	 ,CONCAT(convert(varchar, StartFiscalWeek, 112) , skeyLease) as DatekeyLease
	 ,skeyLease
	  ,SUM(FaceGrpInMkt) as TotalGRPLease
	  ,SUM(FaceGrpInMkt)*7 as WeekTotalGRPLease
 FROM [BI_ODS].[work].[FiscalWeekGRP]
   GROUP BY [StartFiscalWeek],[EndFiscalWeek],FiscalWeekOfYear,skeyLease