﻿


CREATE VIEW [CubeVente360].[PricingAdmanager]
AS

SELECT [SkeyPricingAdmanager]
      ,[FiscalYear]
      ,[Market]
      ,[Lease]
      ,[CampaignType]
      ,[MediaGroup2]
      ,[MediaGroup1]
      ,[AverageDailyFaceGRP]
      ,[DailyGRPCost]
      ,[FaceCost4Weeks]
      ,[DailyFaceCost]
      ,[ProductionCost]
      
  FROM [BI_DWH_AFFICHAGE].[dwh].[DimPricingAdmanager]

  where scd_current = 1