﻿
CREATE VIEW cubevente360.[SalesRep]
AS
SELECT skeySalesRep, SalesRepSapID, SalesRepName1, SalesRepName2, SalesRepCity, SalesRepStreet, SalesRepPostCode, SalesRepRegion, SalesRepCountry, SalesRepPhone1, SalesRepFaxNumber, SalesRepGroup1Sap, SalesRepGroup1, SalesRepGroup2, SalesRepGroup3, 
             SalesRepGroup4, SalesRepGroup5
FROM   BI_DATAQUALITY.mdm.DimSalesRep