﻿


CREATE VIEW [CubeVente360].[RefAdvertiser]
AS
SELECT [skeyRefAdvertiser]
      ,[RefAdvertiserSourceOpe]
      ,[RefAdvertiserOpeSk]
      ,[RefAdvertiserOpeID]
      ,[RefAdvertiserOpeName]
      ,[RefAdvertiserSapOpeID]
      ,[RefAdvertiserLastUpdateOpeDate]
      ,[RefAdvertiserIsLocal]
      ,[skeyAdvertiser]
       FROM [BI_DATAQUALITY].[ref].[DimRefAdvertiser]

	   where scd_current = 1